# Docker image for citeproc-js-server

Runs a [CiteProc JS server](https://github.com/zotero/citeproc-js-server) on port 8085.

For optimal performance, styles and locales are already converted from XML to JSON at build time.

## How to ...

### Use the HTTP API

See https://github.com/zotero/citeproc-js-server#using-the-web-service

### Build and run locally

```shell
docker build -t refstudycentre/citeproc-js-server:latest .
```

```shell
docker run -p 8085:8085 --rm --name citeproc-js-server refstudycentre/citeproc-js-server:latest
```

### Update on hub.docker.com

See [bitbucket-pipelines.yml](bitbucket-pipelines.yml)

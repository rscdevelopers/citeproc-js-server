FROM --platform=$BUILDPLATFORM alpine/git as stage1

# Use rudolfbyker's fork, since the original at https://github.com/zotero/citeproc-js-server.git is unmaintained. 
RUN git clone -b rsc-docker --depth 1 --recurse-submodules https://github.com/rudolfbyker/citeproc-js-server.git /repo
WORKDIR /repo

RUN <<EOF
{
  echo '{'
  echo "  \"commit\": \"$(git rev-parse --verify HEAD)\","
  echo "  \"repo\": \"$(git remote get-url origin)\","
  echo "  \"branch\": \"$(git rev-parse --abbrev-ref HEAD)\""
  echo '}'
} > "git_info.json"
EOF

RUN rm -rf .git test

FROM --platform=$BUILDPLATFORM python:3.11-alpine as prepare

COPY --from=stage1 /repo /repo
WORKDIR /repo

RUN mkdir json && \
    python ./xmltojson.py ./csl ./json/csl && \
    python ./xmltojson.py ./csl-locales ./json/csl-locales && \
    python ./xmltojson.py ./csl/dependent ./json/csl/dependent && \
    cp ./csl/renamed-styles.json ./json/csl/renamed-styles.json

RUN rm -rf csl csl-locales

FROM node:22-alpine3.20

COPY --from=prepare /repo /repo
WORKDIR /repo

RUN npm install

COPY local.json /repo/config/local.json

EXPOSE 8085

CMD ["npm", "start"]
